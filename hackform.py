#! /usr/bin/env python3


import argparse
import csv
import logging
import os
import re
import sys

from lxml import etree


form_re = re.compile('R\d+\.xml$')


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('source_dir', help = 'path of source directory (XML files)')
    parser.add_argument('target_file', help = 'path of target CSV file')
    parser.add_argument('-v', '--verbose', action = 'store_true', default = False, help = 'increase output verbosity')
    args = parser.parse_args()
    logging.basicConfig(level = logging.DEBUG if args.verbose else logging.WARNING, stream = sys.stdout)
    assert os.path.exists(args.source_dir), "Source path doesn't exist: {}.".format(args.source_dir)

    with open(args.target_file, 'w') as csv_file:
        writer = csv.writer(csv_file)
        writer.writerow(['ID', 'NumeroCerfa', 'Type', 'Titre', 'URL', 'Emetteur', 'Audience'])
        for dir, dir_names, filenames in os.walk(args.source_dir):
            for name in dir_names[:]:
                if name.startswith('.'):
                    dir_names.remove(name)
            for filename in filenames:
                if form_re.match(filename) is None:
                    continue
                tree = etree.parse(os.path.join(dir, filename))
                root = tree.getroot()
                support = root.attrib['type']
                if support not in ['Formulaire', 'Téléservice']:
                    continue
                form_link = root.find('LienWeb')
                writer.writerow([
                    root.attrib['ID'],
                    root.findtext('NumeroCerfa') or '',
                    support,
                    root.findtext('{http://purl.org/dc/elements/1.1/}title'),
                    form_link.attrib['URL'] if form_link is not None else '',
                    form_link.findtext('Source') if form_link is not None else '',
                    ', '.join(sorted(audience.text for audience in root.findall('Audience'))),
                    ])
    return 0

if __name__ == "__main__":
    sys.exit(main())
